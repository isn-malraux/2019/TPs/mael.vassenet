#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 29 18:30:24 2018

@author: utilisateur
"""

def double_et_decuple(valeur_a,valeur_b):
    if isinstance(valeur_a,str):
        double = None
    else:
        double = valeur_a*2
    if isinstance(valeur_b,str):
        decuple = None
    else:
        decuple = valeur_b*10
    return (double, decuple)
print(double_et_decuple("a", 9))
