#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 18 19:32:30 2018

@author: utilisateur
"""

import random
nbr_total=int(input("nombre de colonnes et de lignes"))
tab=[[random.randint(100,999) for numero_col in range(nbr_total)] for numero_lgn in range(nbr_total)]
inconnu=0
print(tab)
nombre=int(input("Choisir un nombre"))
for x in range(nbr_total):
    if tab[x].count(nombre)!=0:
        inconnu=1
        print("sa colonne est", tab[x].index(nombre))
        print("sa ligne est", x)
    if inconnu==0:
        print("Le nombre demandé n est pas dans le tableau")